import Head from "next/head";
import Image from "next/image";
import Content from "../comps/Content";
import Sidenav from "../comps/Sidenav";
import TopFilter from "../comps/TopFilter";
import Navbar from "../comps/Navbar";

import products from "../public/products.json";

export default function Home() {
  return (
    <div>
      <Navbar title="Store"></Navbar>
      <TopFilter></TopFilter>
      <div className="flex px-6">
        <Sidenav></Sidenav>
        <Content products={products}></Content>
      </div>
      <div className="banner3 my-8 mx-6 ">
        <Image className="rounded-xl" src="/Banner.png" layout="responsive" width="100vw" height="12vh" />
      </div>
    </div>
  );
}
