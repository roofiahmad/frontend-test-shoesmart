import { useState } from "react";

const Product = ({ product }) => {
  const information = ["description", "detail", "instruction"];
  const [selectInfo, setSelectInfo] = useState();
  const handleSelectInfo = (val) => {
    setSelectInfo(val);
  };
  return (
    <div className="product-section bg-blue-100 col-span-9 rounded-3xl pl-3 pr-6 py-3">
      <div className="grid grid-cols-2">
        <div className="col-span-1 flex justify-center items-center pt-10">
          <img
            className="rounded-xl"
            width="360"
            src="https://s3.ap-southeast-1.amazonaws.com/source-shoesmart-v1.0/thumb_KA6523-881-NAVY.NAVY.2.jpeg"
            // src={product.image_url}
          />
        </div>
        <div className="product-information flex flex-col">
          <div className="flex justify-around items-start">
            {information.map((info) => (
              <button
                onClick={(info) => handleSelectInfo(info)}
                className={`${
                  selectInfo === info ? "bg-black text-white" : "bg-transparent text-black"
                } hover:bg-black hover:text-white font-semibold py-1 px-4 mx-1 my-1  rounded-md text-sm capitalize`}
              >
                {info}
              </button>
            ))}
          </div>
          <div className="item-description bg-white rounded-xl px-4 py-6 mt-1 h-full" dangerouslySetInnerHTML={{ __html: product.description }}></div>
        </div>
      </div>
      <div className="tumbnail grid grid-cols-4 gap-6 mt-6 mb-8">
        <div className="w-full h-40 bg-white rounded-xl flex items-center justify-center">test</div>
        <div className="w-full h-40 bg-white rounded-xl flex items-center justify-center">test</div>
        <div className="w-full h-40 bg-white rounded-xl flex items-center justify-center">test</div>
        <div className="w-full h-40 bg-white rounded-xl flex items-center justify-center">test</div>
      </div>
    </div>
  );
};

export default Product;
