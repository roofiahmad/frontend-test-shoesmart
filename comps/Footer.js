import Head from "next/head";

const Footer = () => {
  return (
    <>
      <Head>
        <link
          rel="stylesheet"
          href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"
          integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm"
          crossorigin="anonymous"
        />
      </Head>
      <div className="footer h-80 w-full bg-indigo-100 rounded-md grid grid-cols-3 justify-center gap-10 pt-16 px-36">
        <div className="brand ">
          <p className="uppercase text-3xl font-semibold mb-8">shoesmart</p>
          <p className="text-sm">
            Marketplace sepatu online untuk wanita dan pria yang didirikan pada 2016. Kami hadir sebagai jawaban atas tantangan dunia teknologi yang
            semakin maju, termasuk dalam hal berbelanja sepatu online
          </p>
        </div>
        <div className="category">
          <p className="uppercase text-xl mb-3 font-medium">Category</p>
          <div className="flex justify-between">
            <div>
              <a className="block text-lg">Boots</a>
              <a className="block text-lg">Flat</a>
              <a className="block text-lg">Heels</a>
              <a className="block text-lg">Loafer</a>
              <a className="block text-lg">Pantofels</a>
            </div>
            <div>
              <a className="block text-lg">About Us</a>
              <a className="block text-lg">FAQ</a>
              <a className="block text-lg">Shiping</a>
              <a className="block text-lg">Return Policy</a>
            </div>
          </div>
        </div>
        <div className="social-media  justify-self-end flex flex-row justify-around items-start	w-2/4">
          <button>
            <i class="fab fa-twitter text-3xl text-gray-800"></i>
          </button>
          <button>
            <i class="fab fa-youtube text-3xl text-gray-800"></i>
          </button>
          <button>
            <i class="fab fa-facebook text-3xl text-gray-800"></i>
          </button>
          <button>
            <i class="fas fa-phone-alt text-2xl text-gray-800 "></i>
          </button>
        </div>
      </div>
    </>
  );
};

export default Footer;
