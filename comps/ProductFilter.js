const ProductFilter = ({ products, handleSort, handleColor, handleSize, sort, color, size }) => {
  const sortLabel = ["ascending", "descending", "lower price", "higher price", "newest", "older"];

  let productsColors = [];
  products.map((product) => productsColors.push(...product.colors));
  let colorLabel = [];
  let colorCode = [];
  productsColors.map((item) => {
    colorLabel.push(item.name);
    colorCode.push(item.rgb);
  });

  colorLabel = new Set(colorLabel);
  colorLabel = [...colorLabel];
  colorCode = new Set(colorCode);

  const sizes = [28, 29, 30, 31, 32, 33, 34, 35, 36, 40];

  let colorClass =
    "bg-transparent hover:bg-black text-gray-300 font-semibold py-1 px-4 mx-1 my-1 border border-gray-300 hover:border-transparent rounded-md text-sm capitalize";

  return (
    <div className="my-4 py-2 px-4 mx-auto bg-white rounded-xl shadow-md w-full">
      <div className="grid grid-cols-4">
        <div className="sort">
          <p className="mx-2 text-xl font-medium mb-1">Sort</p>
          {sortLabel.map((label, index) => (
            <button
              onClick={() => {
                handleSort(label);
              }}
              key={index}
              className={` ${
                sort === label ? "bg-black text-white" : "bg-transparent text-gray-400"
              } hover:bg-black  hover:text-white font-semibold py-1 px-4 mx-1 my-1 border border-gray-300 hover:border-transparent rounded-md text-sm capitalize`}
            >
              {label}
            </button>
          ))}
        </div>
        <div className="color">
          <p className="mx-2 text-xl font-medium mb-1">Color</p>
          {colorLabel.map((label, index) => (
            <button
              onClick={() => {
                handleColor(label);
              }}
              key={index}
              className={
                label === "Black"
                  ? colorClass + " hover:bg-black hover:text-gray-200 "
                  : label === "White"
                  ? colorClass + " hover:bg-white hover:text-black"
                  : colorClass + ` hover:bg-${label.toLowerCase()}-700 hover:text-white `
              }
            >
              {label}
            </button>
          ))}
        </div>
        <div className="size">
          <p className="mx-2 text-xl font-medium mb-1">Size</p>
          {sizes.map((label, index) => (
            <button
              onClick={() => {
                handleSize(label);
              }}
              key={index}
              className={`${
                size === label ? "bg-black text-white" : "bg-transparent text-gray-400"
              } hover:bg-black  hover:text-white font-semibold py-1 px-4 mx-1 my-1 border border-gray-300 hover:border-transparent rounded-md text-sm capitalize`}
            >
              {label}
            </button>
          ))}
        </div>
      </div>
    </div>
  );
};

export default ProductFilter;
