import { useState } from "react";

const Cart = ({ product }) => {
  const sizes = [28, 29, 30, 31, 32, 33, 34, 35, 36, 40];
  let colors = [];
  product.colors.forEach((color) => {
    colors.push(color.name);
  });

  const [size, setSize] = useState();
  const [color, setColor] = useState();
  const [quantity, setQuantity] = useState(0);

  const priceFormater = (price) => {
    return new Intl.NumberFormat(["ban", "id"]).format(price);
  };

  const handleSize = (val) => {
    if (size === val) {
      return setSize("");
    }
    setSize(val);
  };

  const handleColor = (val) => {
    if (color === val) {
      return setColor("");
    }
    setColor(val);
  };

  const handleIncrement = () => {
    let count = quantity;
    count++;
    setQuantity(count);
  };

  const handleDecrement = () => {
    let count = quantity;
    count--;
    setQuantity(count);
  };

  const handleBuy = () => {
    const buyed = {
      product,
      quantity,
      size,
      color,
      total_price: quantity * +product.price,
    };

    console.log(buyed);
  };

  return (
    <div className="product-cart bg-white rounded-3xl pt-6 px-3  pl-5 col-span-3">
      <p className="text-xl font-normal ">{product.name}</p>

      <p className="text-sm font-medium mt-10 mb-1">Size</p>
      {sizes.map((label, index) => (
        <button
          onClick={() => {
            handleSize(label);
          }}
          key={index}
          className={`${
            size === label ? "bg-black text-white" : "bg-transparent text-gray-400"
          } hover:bg-black  hover:text-white font-semibold py-1 px-4 mx-1 my-1 border border-gray-300 hover:border-transparent rounded-md text-sm capitalize`}
        >
          {label}
        </button>
      ))}

      <p className="text-sm font-medium mt-10 mb-1">Color</p>
      {colors.map((label, index) => (
        <button
          onClick={() => {
            handleColor(label);
          }}
          key={index}
          className={`${
            color === label ? "bg-black text-white" : "bg-transparent text-gray-400"
          } hover:bg-black  hover:text-white font-semibold py-1 px-4 mx-1 my-1 border border-gray-300 hover:border-transparent rounded-md text-sm capitalize`}
        >
          {label}
        </button>
      ))}
      <div className="quantity grid grid-cols-6 mt-10 items-center">
        <p className="col-span-2 text-sm font-medium ">Quantity</p>
        <div className="flex col-span-4 justify-between items-center bg-blue-50 rounded-full">
          <button
            onClick={handleDecrement}
            className="bg-white hover:bg-black hover:text-white font-light py-3 px-3 border border-gray-300 hover:border-transparent rounded-full text-2xl"
          >
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20 12H4" />
            </svg>
          </button>
          <p className="text-xl font-normal ">{quantity}</p>
          <button
            onClick={handleIncrement}
            className="bg-white hover:bg-black hover:text-white font-light py-3 px-3 border border-gray-300 hover:border-transparent rounded-full text-2xl"
          >
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4v16m8-8H4" />
            </svg>
          </button>
        </div>
      </div>
      <p className="text-sm font-medium mt-10 ">Price</p>
      <p className="text-xl font-medium mb-1">Rp. {priceFormater(product.price)}</p>
      <div className="action-button flex justify-around items-center mb-5 mt-10">
        <button
          onClick={handleBuy}
          className="hover:bg-black  hover:text-white font-semibold py-1 px-4 mx-1 my-1 border border-gray-300 hover:border-transparent rounded-md text-sm "
        >
          Add to cart
        </button>
        <button
          onClick={handleBuy}
          className="bg-black text-white hover:bg-black hover:text-white font-semibold py-1 px-4 mx-1 my-1 border border-gray-300 hover:border-transparent rounded-md text-xl "
        >
          Buy Now
        </button>
      </div>
    </div>
  );
};

export default Cart;
