import Cart from "../../comps/Cart";
import Navbar from "../../comps/Navbar";
import Preview from "../../comps/Preview";
import Product from "../../comps/Product";
import Card from "../../comps/Card";

import products from "../../public/products.json";

export const getStaticPaths = async () => {
  const paths = products.map((product) => {
    return {
      params: { id: product.id.toString() },
    };
  });

  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps = async (context) => {
  const id = context.params.id;
  const product = products.filter((product) => product.id == id)[0];

  return {
    props: { id, product },
  };
};

const ProductDetail = ({ id, product }) => {
  const relatedProduct = products.slice(0, 5);
  console.log(id, product);

  return (
    <div>
      <Navbar title="Product"></Navbar>
      <div className="product grid grid-cols-12 w-full px-6">
        <Product product={product}></Product>
        <Cart product={product}></Cart>
      </div>
      <Preview></Preview>
      <div className="more-product bg-white mx-6 rounded-xl px-4 py-6 mb-10">
        <p className="text-lg font-semibold">More Products</p>
        <div className="flex justify-around flex-wrap">
          {relatedProduct.map((product) => (
            <a key={product.id} href={"/product-detail/" + product.id}>
              <Card product={product}></Card>
            </a>
          ))}
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;
