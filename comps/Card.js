import Image from "next/image";
import styles from "../styles/Content.module.css";

const Card = ({ product }) => {
  const priceFormater = (price) => {
    return new Intl.NumberFormat(["ban", "id"]).format(price);
  };
  return (
    <div className="card mt-6">
      <div className="py-2 px-4 mx-auto bg-white rounded-xl shadow-md cursor-pointer">
        <div>
          <div className={styles.icon_container}>
            <button>
              <svg xmlns="http://www.w3.org/2000/svg" className={styles.svg_icon} fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={1}
                  d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"
                />
              </svg>
              <span></span>
            </button>
          </div>
          <img
            width="170"
            src="https://s3.ap-southeast-1.amazonaws.com/source-shoesmart-v1.0/thumb_KA6523-881-NAVY.NAVY.2.jpeg"
            // src={product.image_url}
          />
          <p className="text-sm text-gray-600">{product.name}</p>
          <p className="text-xl font-medium text-black">Rp.{priceFormater(product.price)}</p>
        </div>
      </div>
    </div>
  );
};

export default Card;
