import Image from "next/image";

const Preview = () => {
  return (
    <div className="preview grid grid-cols-10 gap-4 w-full px-6 mt-6">
      <div className="col-span-2 rating bg-white rounded-xl h-full flex flex-col justify-center items-center">
        <p className="text-6xl font-medium mb-2">
          5.0<spam className="text-lg text-gray-500 ">/5</spam>
        </p>
        <div className="stars mb-2">
          <i class="fas fa-star text-yellow-500"></i>
          <i class="fas fa-star text-yellow-500"></i>
          <i class="fas fa-star text-yellow-500"></i>
          <i class="fas fa-star text-yellow-500"></i>
          <i class="fas fa-star text-yellow-500"></i>
        </div>
        <p className="font-semibold">Sangat Baik</p>
      </div>
      <div className="col-span-8 comment bg-white rounded-xl px-3 py-3">
        <p className="font-bold text-medium">What they think about this product ?</p>
        <div className="grid grid-cols-12 mt-5">
          <div className="col-span-2 flex flex-col items-center	 ">
            <Image className="rounded-xl" src="/profile.png" width="80" height="80" />
            <div className="stars">
              <i class="fas fa-star text-yellow-500"></i>
              <i class="fas fa-star text-yellow-500"></i>
              <i class="fas fa-star text-yellow-500"></i>
              <i class="fas fa-star text-yellow-500"></i>
              <i class="fas fa-star text-yellow-500"></i>
            </div>
          </div>
          <div className="col-span-10 bg-blue-100 rounded-xl px-2 py-2">
            <p className="font-bold text-medium">Rosalina, Ibu Rumah Tangga</p>
            <p className="float-right text-gray-400 text-sm">Tuesday, 20 June 2019</p>
            <p className="mt-4 text-sm"> Tony Perotti Boots sangat cocok dipakai suami saya, bahannya sangat bagus. Terimakasih Shoesmart!</p>
          </div>
        </div>
      </div>
      <div className="col-span-12  flex justify-center mt-5 my-8">
        <button className="bg-black hover:bg-transparent text-white  hover:text-black py-2 px-6 mx-2 rounded-md">
          See More Review
          <span>
            <i class="fas fa-chevron-down ml-2"></i>
          </span>
        </button>
      </div>
    </div>
  );
};

export default Preview;
