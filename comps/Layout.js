import Footer from "./Footer";

const Layout = ({ children }) => {
  return (
    <div className="bg-gray-100">
      {children}
      <Footer></Footer>
    </div>
  );
};

export default Layout;
