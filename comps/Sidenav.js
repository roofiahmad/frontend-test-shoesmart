import styles from "../styles/Sidenav.module.css";

const Sidenav = () => {
  const products = ["All", "Men", "Women", "Kids"];
  const categories = ["All", "Boots", "Flat", "Heels", "Loafer", "Pantofels", "Sandals", "School Shoes"];
  const informations = ["About Us", "FAQ", "Shippping"];

  return (
    <div className={styles.sidenav}>
      <div className={styles.product}>
        <p className="font-medium text-xl">Product</p>
        {products.map((product, index) => (
          <p className="cursor-pointer text-gray-500 mt-3" key={index}>
            {product}
          </p>
        ))}
      </div>
      <div className={styles.category}>
        <p className="font-medium text-xl">Category</p>
        {categories.map((category, index) => (
          <p className="cursor-pointer text-gray-500 mt-3" key={index}>
            {category}
          </p>
        ))}
        <p className="cursor-pointer text-medium font-medium mt-3">More...</p>
      </div>
      <div className={styles.information}>
        <p className="font-medium text-xl">Information</p>
        {informations.map((information, index) => (
          <p className="cursor-pointer text-gray-500 mt-3" key={index}>
            {information}
          </p>
        ))}
      </div>
    </div>
  );
};

export default Sidenav;
