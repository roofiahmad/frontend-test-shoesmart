import Image from "next/image";
import Card from "../comps/Card";
import Link from "next/link";

const Content = ({ products }) => {
  const bestSellingProduct = products.filter((product) => product.most_sold_product_color_id).slice(0, 10);
  const newProduct = products.sort((a, b) => new Date(b.created_at) - new Date(a.created_at)).slice(0, 10);
  return (
    <div className="content w-full">
      <div className="flex justify-between">
        <div className="banner1">
          <Image src="/Group 45.png" width={520} height={220} />
        </div>
        <div className="banner2">
          <Image src="/Group 46.png" width={520} height={220} />
        </div>
      </div>
      <div className="flex justify-around flex-wrap">
        {bestSellingProduct.map((product) => (
          <a key={product.id} href={"/product-detail/" + product.id}>
            <Card product={product}></Card>
          </a>
        ))}
      </div>
      <div className="banner3 my-8">
        <Image className="rounded-xl" src="/Banner new Coming.png" layout="responsive" width="100vw" height="25vh" />
      </div>
      <div className="flex justify-around flex-wrap">
        {newProduct.map((product) => (
          <a key={product.id} href={"/product-detail/" + product.id}>
            <Card product={product}></Card>
          </a>
        ))}
      </div>
    </div>
  );
};

export default Content;
