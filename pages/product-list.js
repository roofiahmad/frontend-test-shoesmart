import { useState, useEffect } from "react";
import Navbar from "../comps/Navbar";
import ProductFilter from "../comps/ProductFilter";
import Sidenav from "../comps/Sidenav";
import Card from "../comps/Card";
import products from "../public/products.json";

const ProductList = () => {
  const brands = ["All Brands", "Avali", "Zetta", "Barefoot", "Perindu", "Shoelovin", "Misletoe"];
  const [sort, setSort] = useState();
  const [color, setColor] = useState();
  const [size, setSize] = useState();
  const [brand, setBrand] = useState();
  const [filteredProduct, setFilteredProduct] = useState(products);

  const handleSort = (val) => {
    const data = [...products];
    if (sort === val) {
      setFilteredProduct(data);
      return setSort("");
    }
    setSort(val);
    switch (val) {
      case "ascending":
        data.sort((a, b) => a.name - b.name);
        break;
      case "descending":
        data.sort((a, b) => b.name - a.name);
        break;
      case "lower price":
        data.sort((a, b) => Number(a.price) - Number(b.price));
        break;
      case "higher price":
        data.sort((a, b) => Number(b.price) - Number(a.price));
        break;
      case "newest":
        data.sort((a, b) => new Date(b.created_at).getTime() - new Date(a.created_at).getTime());
        break;
      case "older":
        data.sort((a, b) => new Date(a.created_at).getTime() - new Date(b.created_at).getTime());
        break;
      default:
        break;
    }
    setFilteredProduct(data);
  };

  const handleColor = (val) => {
    setColor(val);
    const data = [...products];
    const temp = data.filter((product) => {
      let res;
      product.colors.forEach((color, index) => {
        if (color.name === val) return (res = true);
        if (index === product.colors.length - 1) return (res = false);
      });
      return res;
    });
    setFilteredProduct(temp);
  };

  const handleSize = (val) => {
    const data = [...products];

    if (size === val) {
      setFilteredProduct(data);
      return setSize("");
    }
    setSize(val);
    const temp = data.filter((product) => {
      let res = false;
      // console.log("variants", product.variants);
      product.variants.forEach((variant, index) => {
        // console.log("variant", variant.sizes);
        variant.sizes.forEach((size) => {
          // console.log(size.size === val);
          if (size.size === val) return (res = true);
        });
        // if (index === product.variants.length - 1) return (res = false);
      });
      return res;
    });
    setFilteredProduct(temp);
  };

  const handleSelectBrand = (val) => {
    if (val === brand) return setBrand("");
    setBrand(val);
  };

  useEffect(() => {
    console.log("products", products);
  }, []);

  return (
    <div>
      <Navbar title="Product List"></Navbar>
      <div className="flex px-6 mt-8">
        <Sidenav></Sidenav>
        <div className="list-filter pl-5 w-full">
          <div className="search mb-3">
            <input className="py-2 px-4 w-3/4 text-gray-500 rounded-md" type="text" placeholder="Search your favorites shoes ..."></input>
          </div>
          <div className="brand-filter  w-full">
            {brands.map((label, index) => (
              <button
                onClick={() => handleSelectBrand(label)}
                key={index}
                className={`${
                  brand === label ? "bg-black text-white" : "bg-white text-gray-500 "
                } hover:bg-black text-sm font-normal hover:text-white py-2 px-6 rounded-md mx-1`}
              >
                {label}
              </button>
            ))}
            <button className="ml-2 text-xs font-semibold">More Brands > </button>
          </div>
          <ProductFilter
            sort={sort}
            color={color}
            size={size}
            handleSort={handleSort}
            handleColor={handleColor}
            handleSize={handleSize}
            products={products}
          ></ProductFilter>
          <div className="display-products grid grid-cols-5 gap-x-3">
            {filteredProduct.map((product) => (
              <a key={product.id} href={"/product-detail/" + product.id}>
                <Card product={product}></Card>
              </a>
            ))}
          </div>
          <div className="more-product flex justify-center mt-10 mb-16 text-sm ">
            <button className="bg-black hover:bg-transparent text-white  hover:text-black py-2 px-6 mx-2 rounded-md">
              See More Product{" "}
              <span>
                <i class="fas fa-chevron-down ml-2"></i>
              </span>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductList;
