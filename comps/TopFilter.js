import { useState } from "react";

const TopFilter = () => {
  const [brand, setBrand] = useState();
  const brands = ["All Brands", "Avali", "Zetta", "Barefoot", "Perindu", "Shoelovin", "Misletoe"];

  const handleSelectBrand = (val) => {
    if (val === brand) return setBrand("");
    setBrand(val);
  };

  return (
    <div className="flex justify-between px-6 h-24 items-center">
      <div className="brand-filter">
        {brands.map((label, index) => (
          <button
            onClick={() => handleSelectBrand(label)}
            key={index}
            className={`${
              brand === label ? "bg-black text-white" : "bg-white text-gray-500 "
            } hover:bg-black text-sm font-normal hover:text-white py-2 px-6 rounded-md mx-1`}
          >
            {label}
          </button>
        ))}
        <button className="ml-2 text-xs font-semibold">More Brands > </button>
      </div>
      <div className="search">
        <input className="py-2 px-4 text-gray-500 rounded-md" type="text" placeholder="Search your favorites shoes ..."></input>
      </div>
    </div>
  );
};

export default TopFilter;
