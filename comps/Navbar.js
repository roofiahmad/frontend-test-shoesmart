import Head from "next/head";

const Navbar = ({ title }) => {
  return (
    <>
      <Head>
        <link
          rel="stylesheet"
          href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"
          integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm"
          crossorigin="anonymous"
        />
      </Head>
      <div className="navbar bg-gray-50 flex justify-between px-6 py-3">
        <div className="logo flex">
          <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 11V7a4 4 0 00-8 0v4M5 9h14l1 12H4L5 9z" />
          </svg>
          <p className="ml-1 text-lg font-medium">{title}</p>
        </div>
        <div className="brand text-2xl uppercase font-bold">
          <p>shoesmart</p>
        </div>
        <div className="button-account">
          <button className="bg-transparent hover:bg-black text-black font-semibold hover:text-white py-1 px-6 hover:border-transparent rounded-md">
            Login
          </button>
          <button className="bg-black hover:bg-transparent text-white font-semibold hover:text-black py-1 px-6 mx-2 rounded-md">Register</button>
        </div>
      </div>
    </>
  );
};

export default Navbar;
